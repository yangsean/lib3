//
//  main.m
//  lib3
//
//  Created by yanghonglei on 05/12/2016.
//  Copyright (c) 2016 yanghonglei. All rights reserved.
//

@import UIKit;
#import "ymAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ymAppDelegate class]));
    }
}
