//
//  ymAppDelegate.h
//  lib3
//
//  Created by yanghonglei on 05/12/2016.
//  Copyright (c) 2016 yanghonglei. All rights reserved.
//

@import UIKit;

@interface ymAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
